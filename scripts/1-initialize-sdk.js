import { ThirdwebSDK } from "@thirdweb-dev/sdk";
import { ethers } from "ethers";
import dotenv from "dotenv";
dotenv.config();

const provider = new ethers.providers.JsonRpcProvider(process.env.ALCHEMY_API_URL);
const wallet = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
const sdk = new ThirdwebSDK(wallet);

( async () => {
    try {
        const address = await sdk.getSigner().getAddress();
        console.log("SDK Address - ", address);
    } catch (error) {
        console.log("Failed to initialise");
        process.exit(1);
    }
})();


export default sdk;