import sdk from "./1-initialize-sdk.js";
import { readFileSync } from "fs";

const editionDrop = sdk.getEditionDrop("0xb204919c81E7D3Cd7f57C1acfd71AF0865c008F7");

(async () => {
  try {
    await editionDrop.createBatch([
      {
        name: "Dragan Ball Headband",
        description: "This NFT will give you access to DragonBallDAO!",
        image: readFileSync("scripts/assets/batch.jpg"),
      },
    ]);
    console.log("✅ Successfully created a new NFT in the drop!");
  } catch (error) {
    console.error("failed to create the new NFT", error);
  }
})();